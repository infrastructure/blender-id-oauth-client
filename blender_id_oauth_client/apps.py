from django.apps import AppConfig


class BlenderIDOAuthConfig(AppConfig):
    name = 'blender_id_oauth_client'
    verbose_name = 'Blender ID OAuth'
    default_auto_field = 'django.db.models.AutoField'
