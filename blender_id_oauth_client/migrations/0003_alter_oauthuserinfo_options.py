# Generated by Django 3.2.11 on 2022-01-10 16:06

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blender_id_oauth_client', '0002_oauth_user_info'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='oauthuserinfo',
            options={'verbose_name_plural': 'OAuth User info'},
        ),
    ]
