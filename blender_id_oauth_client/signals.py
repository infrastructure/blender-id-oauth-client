from django.dispatch import Signal

user_created = Signal()
"""Sent when a user is created by logging in via Blender ID.

:param instance: the user model instance.
:param oauth_info: the info dict received from Blender ID, contains
    'id', 'nickname', 'email', 'full_name', and possibly other keys.
"""


id_token_received = Signal()
"""Sent after a user is logged in via OpenID Connect.

:param instance: the user model instance.
:param request: current request instance
:param id_token: raw undecoded id_token payload
"""
