import urllib.parse

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
import responses

from blender_id_oauth_client import models

httpmock = responses.RequestsMock()
User = get_user_model()


class OAuthLoginTest(TestCase):

    def oauth_login(self, user_email, user_nickname, user_id=47):
        next_after_login = 'https://localhost:12345/gimme_token'
        next_query = urllib.parse.urlencode({'next': next_after_login})
        login_url = reverse('oauth:login') + f'?{next_query}'

        resp = self.client.get(login_url)
        self.assertEqual(302, resp.status_code)

        redirect_uri = self.parse_redir_uri(resp)
        oauth_state = self.client.session['oauth_state']

        httpmock.add('POST', 'https://id.local:8400/oauth/token', json={
            'access_token': 'mocked-access-token',
            'refresh_token': 'mocked-refresh-token',
            'expires_in': 300,  # seconds
        })
        httpmock.add('GET', 'https://id.local:8400/api/me', json={
            'email': user_email,
            'full_name': "Harry 'local user' de Bøker",
            'id': user_id,
            'nickname': user_nickname,
            'roles': {
                'cloud_has_subscription': True,
                'cloud_subscriber': True,
                'fund_supporter': True,
            }
        })
        # Mimick Blender ID redirecting us back to the redirect_uri.
        resp = self.client.get(f'{redirect_uri}?code=MOCKED_GRANT_CODE&state={oauth_state}')
        self.assertEqual(302, resp.status_code)
        self.assertEqual(next_after_login, resp['Location'])

        httpmock.remove('POST', 'https://id.local:8400/oauth/token')
        httpmock.remove('GET', 'https://id.local:8400/api/me')

    def parse_redir_uri(self, login_response) -> str:
        parsed_redir_url = urllib.parse.urlparse(login_response['Location'])
        parsed_qs = urllib.parse.parse_qs(parsed_redir_url.query)
        redirect_uri = parsed_qs['redirect_uri'][0]
        return redirect_uri

    @httpmock.activate
    def test_authorize_redirect(self):
        r = self.client.get(reverse('oauth:login'))
        self.assertEqual(302, r.status_code)
        self.assertTrue(r['Location'].startswith('https://id.local:8400/oauth/authorize?'),
                        r['Location'])

        parsed_redir_url = urllib.parse.urlparse(r['Location'])
        parsed_qs = urllib.parse.parse_qs(parsed_redir_url.query)

        callback_url = urllib.parse.urljoin('http://testserver/', reverse('oauth:callback'))
        self.assertEqual({'client_id': ['TEST-CLIENT-ID'],
                          'redirect_uri': [callback_url],
                          'response_type': ['code'],
                          'state': [self.client.session['oauth_state']]},
                         parsed_qs)

    @httpmock.activate
    def test_state_mismatch(self):
        next_after_login = 'https://localhost:12345/gimme_token'
        next_query = urllib.parse.urlencode({'next': next_after_login})
        login_url = reverse('oauth:login') + f'?{next_query}'

        resp = self.client.get(login_url)
        self.assertEqual(302, resp.status_code)

        redirect_uri = self.parse_redir_uri(resp)

        session = self.client.session
        original_state = session['oauth_state']
        session['oauth_state'] = 'altered-state'
        session.save()

        # Mimick Blender ID redirecting us back to the redirect_uri.
        resp = self.client.get(f'{redirect_uri}?code=MOCKED_GRANT_CODE&state={original_state}')
        self.assertEqual(302, resp.status_code)
        self.assertEqual(login_url, resp['Location'])

        self.assertNotIn('oauth_state', self.client.session)
        self.assertNotIn('next_after_login', self.client.session)

        # Mimick redoing the login.
        resp = self.client.get(login_url)
        self.assertEqual(302, resp.status_code)

        new_state = self.client.session['oauth_state']
        self.assertNotEqual('altered-state', new_state)
        self.assertNotEqual(original_state, new_state)
        self.assertEqual(next_after_login, self.client.session['next_after_login'])

    @httpmock.activate
    def test_callback_in_another_session(self):
        resp = self.client.get(reverse('oauth:callback'))
        self.assertEqual(302, resp.status_code)
        self.assertEqual(reverse('oauth:login') + '?next=%2F', resp['Location'])

    @httpmock.activate
    def test_grant_invalid_retry(self):
        # GET the login page to start a session.
        resp = self.client.get(reverse('oauth:login'))
        self.assertEqual(302, resp.status_code)

        redirect_uri = self.parse_redir_uri(resp)
        oauth_state = self.client.session['oauth_state']

        httpmock.add('POST', 'https://id.local:8400/oauth/token', json={
            'error': 'invalid_grant',
            'error_description': 'too slow!',
        }, status=400)

        # Mimick Blender ID redirecting us back to the redirect_uri.
        resp = self.client.get(f'{redirect_uri}?code=MOCKED_GRANT_CODE&state={oauth_state}')
        self.assertEqual(302, resp.status_code)
        self.assertEqual(reverse('oauth:login'), resp['Location'])

    @httpmock.activate
    def test_grant_invalid_loop_detected(self):
        # GET the login page to start a session.
        resp = self.client.get(reverse('oauth:login'))
        self.assertEqual(302, resp.status_code)

        session = self.client.session
        redirect_uri = self.parse_redir_uri(resp)
        oauth_state = session['oauth_state']

        httpmock.add('POST', 'https://id.local:8400/oauth/token', json={
            'error': 'invalid_grant',
            'error_description': 'too slow!',
        }, status=400)

        # Mimick that we've already looped a few times
        session['bid_login_retry_count'] = 3
        session.save()

        # Mimick Blender ID redirecting us back to the redirect_uri.
        resp = self.client.get(f'{redirect_uri}?code=MOCKED_GRANT_CODE&state={oauth_state}')
        self.assertEqual(302, resp.status_code)
        self.assertEqual(reverse('oauth:login_loop_detected'), resp['Location'])

    @httpmock.activate
    def test_create_local_user(self):
        self.assertEqual(0, len(User.objects.all()))
        user_email = 'harry@blender.org'
        user_nickname = 'Bøker'

        self.oauth_login(user_email, user_nickname)

        # A new local user should have been created.
        new_user: User = User.objects.first()
        self.assertEqual(user_email, new_user.email)
        self.assertEqual(user_nickname, new_user.username)

    @httpmock.activate
    def test_match_by_email(self):
        user_email = 'harry@blender.org'
        user_nickname = 'Bøker'

        # Make sure a local user with the given email address already exists
        User.objects.create(email=user_email, username=user_nickname)
        self.assertEqual(1, len(User.objects.all()))

        self.oauth_login(user_email, user_nickname)

        # No new users should have been created
        self.assertEqual(1, len(User.objects.all()))
        user: User = User.objects.get(email=user_email)
        self.assertEqual(user_nickname, user.username)

    @httpmock.activate
    def test_change_email(self):
        # Login once, this creates the local user.
        self.oauth_login('harry@blender.org', 'Bøker')
        user: User = User.objects.get(email='harry@blender.org')

        # Login again, but with a changed email address and nickname.
        # This should update the local user, rather than trying to create a new one.
        self.oauth_login('harry+changed@blender.org', 'Bøkertje')

        # The old OAuth token from before the email change still exists in the database,
        # and that should be fine.

        # No new users should have been created
        self.assertEqual(1, len(User.objects.all()))
        user.refresh_from_db()
        self.assertEqual('harry+changed@blender.org', user.email)
        self.assertEqual('Bøkertje', user.username)

        # When all the tokens have been forgotten, the Blender ID UID
        # should still be matched to this user.
        models.OAuthToken.objects.all().delete()
        self.oauth_login('harry+changed-again@blender.org', 'Kabats')

        # Still we should have only one user.
        self.assertEqual(1, len(User.objects.all()))
        user.refresh_from_db()
        self.assertEqual('harry+changed-again@blender.org', user.email)
        self.assertEqual('Kabats', user.username)

    @httpmock.activate
    def test_duplicate_username_on_create(self):
        # Login once, this creates the local user.
        self.oauth_login('harry@blender.org', 'Bøker')
        user: User = User.objects.get(email='harry@blender.org')

        # Login again, but with a different user having the same username.
        # This should create another local user with a username that has a suffix.
        self.oauth_login('john@blender.org', 'Bøker', user_id=999)

        # Two new users should have been created
        self.assertEqual(2, len(User.objects.all()))

        user.refresh_from_db()
        self.assertEqual('harry@blender.org', user.email)
        self.assertEqual('Bøker', user.username)

        another_user = User.objects.get(oauth_info__oauth_user_id=999)
        self.assertEqual('john@blender.org', another_user.email)
        self.assertTrue(another_user.username.startswith('Bøker_'))

    @httpmock.activate
    def test_duplicate_username_on_update(self):
        # Login once, this creates the local user.
        self.oauth_login('harry@blender.org', 'Bøker')
        user: User = User.objects.get(email='harry@blender.org')

        # Login again, but with a different user.
        self.oauth_login('john@blender.org', 'Not-Yet-Bøker', user_id=999)

        # Two new users should have been created
        self.assertEqual(2, len(User.objects.all()))

        user.refresh_from_db()
        self.assertEqual('harry@blender.org', user.email)
        self.assertEqual('Bøker', user.username)

        another_user = User.objects.get(oauth_info__oauth_user_id=999)
        self.assertEqual('john@blender.org', another_user.email)
        self.assertEqual('Not-Yet-Bøker', another_user.username)
        # self.assertTrue(another_user.username.startswith('Bøker_'))

        # Login again with the second user, but now with a duplicate username.
        self.oauth_login('john@blender.org', 'Bøker', user_id=999)

        # No users should have been created
        self.assertEqual(2, len(User.objects.all()))
        user.refresh_from_db()
        another_user.refresh_from_db()

        self.assertEqual('harry@blender.org', user.email)
        self.assertEqual('Bøker', user.username)
        self.assertEqual('john@blender.org', another_user.email)
        # The username changed to a duplicate with a suffix
        self.assertTrue(another_user.username.startswith('Bøker_'))
