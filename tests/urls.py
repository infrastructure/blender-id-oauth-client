from django.conf.urls import include
from django.urls import path

import blender_id_oauth_client.urls

from . import views

urlpatterns = [
    path('protected', views.protected_view, name='protected-view'),
    path('oauth/', include(blender_id_oauth_client.urls)),
]
